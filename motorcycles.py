motorcycles = ['honda', 'yamaha', 'suzuki']
print(motorcycles)
# motorcycles[0]= 'ducati'
# print(motorcycles)
motorcycles.append('ducati')
print(motorcycles)
motorcycles.insert(0, 'YaMaHa')
print(motorcycles)
del motorcycles[0]
print(motorcycles)
del motorcycles[1]
print(motorcycles)
motorcycles.pop()
print(motorcycles)
print("The last motorcycles I owned was a " + motorcycles.pop())
motorcycles = ['honda', 'yamaha', 'suzuki']
motorcycles.remove("yamaha")
print(motorcycles)
print("\nA " + 'ducati'.title() + ' is too expensive for me.')
