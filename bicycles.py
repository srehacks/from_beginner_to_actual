bicycles = ['trek', 'cannondale', 'redline', 'specialized']
print(bicycles)
print(bicycles[0].title())
print(bicycles[1])
print(bicycles[3])
print(bicycles[-1])
message = "My first bicycle was a " + bicycles[0].title() + "."
print(message)

names = ['lucy', 'lily', 'yuki', 'amy']
print(names[0])
print(names[1])
print(names[2])
print(names[3])
print("Hello, " + names[0])
print("Hello, " + names[1])
print("Hello, " + names[2])
print("Hello, " + names[3])
message = "I would like to own a Yamaha motorcycle"
print(message)
