name = 'eric'
message = ", would you like to learn some Python today?"
print("Hello " + name.title() + message)

first_name = "bruce"
last_name = "lee"
full_name = first_name + " " + last_name
print(full_name.title())
print(full_name.upper())
print(full_name.lower())

message = "A person who never made a mistake never tried anything new."
print(message)

famous_person = "Albert Einstein once said, "

print(famous_person + message)

name = "  \tsteven\njonas   "
print(name.lstrip())
print(name.rstrip())
print(name.strip())

honored_guest = ['Mike', 'Michael', 'Mac', 'Max']
print(honored_guest)
honored_guest.insert(0, 'May')
print(honored_guest)
honored_guest.insert(2, 'Mark')
print(honored_guest)
honored_guest.append('Marry')
print(honored_guest)
honored_guest.pop()
print(honored_guest)
del honored_guest[-1]
print(honored_guest)
del honored_guest[-1]
print(honored_guest)

place = ['beijing', 'shanghai', 'guangzhou']
print(sorted(place))
print(sorted(place, reverse=True))
print(place)
place.reverse()
print(place)
place.reverse()
print(place)
place.sort()
print(place)
place.sort(reverse=False)
print(place)
print(len(honored_guest))
