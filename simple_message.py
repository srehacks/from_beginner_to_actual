message = "Hello Python Crash Course reader!"
print(message)

name = "ada lovelace"
print(name.title())
print(name.upper())
print(name.lower())

first_name = 'ada'
last_name = ' lovelace'
full_name = first_name + last_name
print(full_name)
print("Hello, " + full_name.title() + "!")

print("\tpython")
print("Lanuages:\nPython\nC\nJavaScript!")

favorite_language = '  python  '
print(favorite_language)
print(favorite_language.rstrip())
print(favorite_language.lstrip())
print(favorite_language.strip())
